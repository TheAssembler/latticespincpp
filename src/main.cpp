/**********************************
Copyright (c) 2008 Arjun R. Acharya.
All rights reserved. Use of the code is allowed under the
Artistic License 2.0 terms, as specified in the LICENSE file
distributed with this code, or available from
http://www.opensource.org/licenses/artistic-license-2.0.php
***********************************/

#include <iostream>
#include <time.h>
#include <math.h>
#include "derived_latticespin.h"
#include <cstdlib>
using namespace std;

//#define _SIM3D
//#define _RANGE_OF_SIMS //If this is defined the simulation
//is performed for a range of temperatures in a single run
#ifdef _RANGE_OF_SIMS

#define _SPECIFIC_HEAT
//Now we have to specify which variable is being varied.
//#define _TEMPERATURE
#define _MAGNETIC

#endif
//***************************************
//***************CONSTANTS***************
//***************************************
const int dimofspin = 2;
const int sidelength = 4;

const double J = 1.0;
const double mu = 0.5;
//const double kT = 3.0;
const double kT = 2;

const double specific_heat_perturbation = 1.025;
//This denotes the amount the temperature is
//multiplied by in order to get the perturbed
//temperature, which is then used to calculate the
//specific heat.

#ifdef _SIM3D
double mag[] = {-1.5,0.,0.};
std::vector<double> magfield(mag,mag+3);
#else
double mag[] = {0.,0.};
std::vector<double> magfield(mag,mag+2);
#endif

//const int sweeps_init = 50000;
//const int sweeps_main = 1000000;
//const int check_and_output = 50000;

const int sweeps_init = 1000;
const int sweeps_main = 100000;
const int check_and_output = 5000;

const int rate_of_record = 1;//Once every "rate_of_record" sweeps the data is stored in output file
//***************************************
//***************END OF CONSTANTS********
//***************************************
void record_params(void);
void range_of_sims_at_diff_temps(double start_t, double end_t, int num_exp);
void single_run(void);
//**************************
//**************************
//**************************
class mytimer
{
    clock_t t_start;
    clock_t t_end;
public :
    void start();
    void end();
};
//**************************
//**************************
//**************************
int main(void)
{
    mytimer myclock;
    myclock.start();

    record_params();

#ifdef _RANGE_OF_SIMS

#ifdef _TEMPERATURE
#ifdef _MAGNETIC
    throw exception("Both _TEMPERATURE and _MAGNETIC cannot be simultaneously defined\n");
#endif
#endif

    double start_t = 0.0;
    double end_t = 0.5;
    int num_exp = 6;

    range_of_sims_at_diff_temps(start_t,end_t,num_exp);

#else //Else continuing from "#ifdef _RANGE_OF_SIMS"
    single_run();
#endif //#ifndef _RANGE_OF_SIMS

    myclock.end();
    cout<<"Finished\n";
    cin.get();
}
//**************************
//**************************
//**************************
void record_params(void)
{

    ofstream out_params("parameters_of_sim.txt");
    out_params<<"sidelength = "<<sidelength<<endl;

#ifdef _SIM3D
    out_params<<"system dimension = "<<3<<endl;
    out_params<<"mag[0] = "<<mag[0]<<endl;
    out_params<<"mag[1] = "<<mag[1]<<endl;
    out_params<<"mag[2] = "<<mag[2]<<endl;
#else
    out_params<<"system dimension = "<<2<<endl;
    out_params<<"mag[0] = "<<mag[0]<<endl;
    out_params<<"mag[1] = "<<mag[1]<<endl;
#endif

    out_params<<"spin dimension = "<<dimofspin<<endl;
    out_params<<"J = "<<J<<endl;
    out_params<<"mu = "<<mu<<endl;
    out_params<<"kT = "<<kT<<endl;
    out_params<<"sweeps_init = "<<sweeps_init<<endl;
    out_params<<"sweeps_main = "<<sweeps_main<<endl;
    out_params<<"rate_of_record = "<<rate_of_record<<endl;
    out_params<<"specific_heat_perturbation = "<<specific_heat_perturbation<<endl;

#ifdef _TEST
    out_params<<"_TEST preprocessor defined\n"<<endl;
#endif

    out_params.close();
}
//**************************
//**************************
//**************************
#ifdef _RANGE_OF_SIMS
void range_of_sims_at_diff_temps(double start_t, double end_t, int num_exp)
{
    //We obtain the analytic values for the average
    //internal energies for the range of temperatures

    clock_t en, es, ec, e0, ef;
    double mult;

#ifndef _SPECIFIC_HEAT
    mult = 1.;
#else
    mult = 2.;
#endif

    e0 = clock();
    double time_to_end;
    /*
    A note on the estimation of time. Suppose that the current sweep = j,
    and the current iteration is equal to i. Then :

    note that we ignore the equilibration bit

    IF WE ARE NOT IN THE SPECIFIC_HEAT section, then :

    number of iterations passed = 2*i
    number of iterations left = 2*(num_exp+1)

    Suppose that :

    en = current time now
    es = time when the current iteration was commenced
    ec = expected time when the current iteration finishes
    e0 = beginning of the whole experiement
    ef = expected time when the whole experiment finishes

    Then we have :

    ec = (sweeps_main/j)*(en-es) + es
    ef = ((2*(num_exp+1))/(2*j+1))*(ec-e0) + e0

    duration remaining = (ef - en)/CLOCKS_PER_SEC

    IF WE ARE IN THE SPECIFIC_HEAT section, then we have :

    ec = (sweeps_main/j)*(en-es) + es
    ef = ((2*(num_exp+1))/(2*j+2))*(ec-e0) + e0

    duration remaining = (ef - en)/CLOCKS_PER_SEC

    If the SPECIFIC_HEAT preprocessor is not defined, then
    we have the following :

    ec = (sweeps_main/j)*(en-es) + es
    ef = ((num_exp+1)/(j+1))*(ec-e0) + e0

    i.e.
    The two cases can be dealt with via the introduction of a multiplier:
    if SPECIFIC_HEAT defined, then mult = 2, otherwise mult=1, and we have :

    ec = (sweeps_main/j)*(en-es) + es
    ef = ((mult*(num_exp+1))/(mult*j+1))*(ec-e0) + e0

    for the section which is outside the specific heat section
    */
    ofstream out_range("output_range_of_sims_params.txt");

    //double start_t = 3;
    //double end_t = 10;
    //int num_exp = 8;

    double width = (end_t-start_t)/num_exp;

    out_range<<"start_t = "<<start_t<<endl;
    out_range<<"end_t = "<<end_t<<endl;
    out_range<<"num_exp = "<<num_exp<<endl;
    out_range<<"width = "<<width<<endl;

    out_range.close();

#ifndef _SIM3D
#ifdef _TEMPERATURE
    ofstream out_analy("output_energies_analytic_range.txt");
    for (int i=0; i<= num_exp; ++i)
    {
        double localvar = start_t + i*width;
        ising2d<sidelength,dimofspin> mytest(
            J,
            mu,
            magfield,
            localvar
        );
        out_analy<<localvar<<"\t"<<mytest.analytical_internal_energy_per_particle()<<endl;
    }
    out_analy.close();
#endif
#endif

    //We now do the Metropolis simulations in order to
    //obtain the internal energies, which can then be
    //compared to the theoretical values
    for (int i=0; i <= num_exp ; ++i)
    {

        //Creating the name of the storage file
        char sst[] = "output_energies_";
        char eet[100];
        _itoa_s(i,eet,100,10);
        char * tt = eet;
        strcat(sst,tt);
        strcat(sst,".txt");


        ofstream out(sst);

        //denotes the quantity which is being varied
        double localvar = start_t + i*width;

#ifdef _TEMPERATURE
#ifdef _SIM3D
        ising3d<sidelength,dimofspin> mytest(
            J,
            mu,
            magfield,
            localvar
        );
#else
        ising2d<sidelength,dimofspin> mytest(
            J,
            mu,
            magfield,
            localvar
        );
#endif
#endif

#ifdef _MAGNETIC
#ifdef _SIM3D
        ising3d<sidelength,dimofspin> mytest(
            J,
            localvar,
            magfield,
            kT
        );
#else
        ising2d<sidelength,dimofspin> mytest(
            J,
            localvar,
            magfield,
            kT
        );
#endif
#endif

        //Initialisation
        for (int j=0; j<sweeps_init; ++j)
        {
            mytest.latticeupdate_sitespecified();
        }
        es = clock();
        //Main run
        for (int j=1; j<sweeps_main; ++j)
        {
            if (j%check_and_output == 0)
            {
                mytest.check();
                cout<<"iteration = "<<i<<" sweep = "<<j<<endl;
                en = clock();

                ec = clock_t((double(sweeps_main)/double(j))*double(en-es)) +es;
                ef = clock_t((mult*(num_exp+1.)/(mult*i+1))*double(ec-e0)) + e0;

                time_to_end = double((ef - en)/CLOCKS_PER_SEC);
                cout<<"Estimated time left = "<<time_to_end / 60.<<" minutes"<<endl;
            }
            mytest.latticeupdate_sitespecified();
            if (j%rate_of_record == 0)
            {
                out<<mytest.getenergy();
                for (int l_dim = 0; l_dim < dimofspin; ++l_dim)
                {
                    out<<"\t"<<mytest.getmagnetisation(l_dim);
                }
                out<<endl;
            }
        }
        out.close();

#ifdef _SPECIFIC_HEAT
        //***START OF SIMULATION TO ALLOW ONE TO CALCULATE SPECIFIC HEAT
        clock_t now_specific = clock();
        {

            char sst2[] = "output_energies_specific_heat_";
            char eet2[100];
            _itoa_s(i,eet2,100,10);
            char * tt2 = eet2;
            strcat(sst2,tt2);
            strcat(sst2,".txt");

            ofstream out_specific(sst2);
#ifdef _TEMPERATURE
#ifdef _SIM3D
            ising3d<sidelength,dimofspin> mytest(
                J,
                mu,
                magfield,
                localvar*specific_heat_perturbation
            );
#else
            ising2d<sidelength,dimofspin> mytest(
                J,
                mu,
                magfield,
                localvar*specific_heat_perturbation
            );
#endif
#endif

#ifdef _MAGNETIC
#ifdef _SIM3D
            ising3d<sidelength,dimofspin> mytest(
                J,
                localvar*specific_heat_perturbation,
                magfield,
                kT
            );
#else
            ising2d<sidelength,dimofspin> mytest(
                J,
                localvar*specific_heat_perturbation,
                magfield,
                kT
            );
#endif
#endif

            //Initialisation
            for (int j=0; j<sweeps_init; ++j)
            {
                mytest.latticeupdate_sitespecified();
            }
            es = clock();
            //Main run
            for (int j=1; j<sweeps_main; ++j)
            {
                if (j%check_and_output == 0)
                {
                    mytest.check();
                    cout<<"iteration = "<<i<<" sweep = "<<j<<endl;
                    en = clock();

                    ec = clock_t((double(sweeps_main)/double(j))*double(en-es)) +es;
                    ef = clock_t((2*(num_exp+1.)/(2*i+2.))*double(ec-e0)) + e0;

                    time_to_end = double((ef - en)/CLOCKS_PER_SEC);
                    cout<<"Estimated time left = "<<time_to_end / 60.<<" minutes"<<endl;
                }
                mytest.latticeupdate_sitespecified();
                if (j%rate_of_record == 0)
                {
                    out_specific<<mytest.getenergy();
                    for (int l_dim = 0; l_dim < dimofspin; ++l_dim)
                    {
                        out_specific<<"\t"<<mytest.getmagnetisation(l_dim);
                    }
                    out_specific<<endl;
                }
            }

            out_specific.close();

        }
        //***END OF SIMULATION TO ALLOW ONE TO CALCULATE SPECIFIC HEAT
#endif//for(int i=0; i <= num_exp ; ++i){
    }
}
#endif
//**************************
//**************************
//**************************
void single_run(void)
{
    clock_t start = clock();
    clock_t now, end;
    double time_to_end;
    ofstream out("output_energies_single_run.txt");

    try
    {
#ifdef _SIM3D
        ising3d<sidelength,dimofspin> mytest(
            J,
            mu,
            magfield,
            kT
        );
#else
        ising2d<sidelength,dimofspin> mytest(
            J,
            mu,
            magfield,
            kT
        );
        cout<<"Internal energy (analytic, perparticle) = "<<mytest.analytical_internal_energy_per_particle()<<endl;
        cout<<"Ground state = "<<mytest.calcglobalenergy()<<endl;
#endif
        try
        {
            for (int i=0; i<sweeps_init; ++i)
            {
                mytest.latticeupdate_sitespecified();
            }
            start = clock();
            for (int i=1; i<sweeps_main; ++i)
            {
                if (i%check_and_output == 0)
                {
                    mytest.check();
                    cout<<"sweep = "<<i<<endl;
                    now = clock();
                    end = clock_t((double(sweeps_main)/double(i)) * double(now-start) + double(start));
                    time_to_end = double((end - now)/CLOCKS_PER_SEC);
                    cout<<"Estimated time left = "<<time_to_end / 60.<<" minutes"<<endl;
                }
                mytest.latticeupdate_sitespecified();
                if (i%rate_of_record == 0)
                {
                    out<<mytest.getenergy();
                    for (int l_dim = 0; l_dim < dimofspin; ++l_dim)
                    {
                        out<<"\t"<<mytest.getmagnetisation(l_dim);
                    }
                    out<<endl;
                }
            }
        }
        catch (exception& expp)
        {
            cout<<expp.what();
            cin.get();
        }
    }
    catch (exception& expp)
    {
        cout<<expp.what();
        cin.get();
    }
    out.close();
}
//**************************
//**************************
//**************************
void mytimer::start()
{
    t_start = clock();
}

void mytimer::end()
{
    t_end = clock();
    double seconds = (t_end-t_start)/CLOCKS_PER_SEC;
    double minutes = seconds / 60.;
    double hours = minutes / 60.;
    cout<<endl<<"number of seconds simulation took = "<<seconds<<endl<<endl;
    cout<<endl<<"number of minutes simulation took = "<<minutes<<endl<<endl;
    cout<<endl<<"number of hours simulation took = "<<hours<<endl<<endl;
}
//**************************
//**************************
//**************************

//This is a workaround for some strange behaviour which
//is observed when Visual Studio Language extensions
//is turned off. Make sure that it remains at the bottom
//of the code
namespace
{

};
