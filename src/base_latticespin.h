/**********************************
Copyright (c) 2008 Arjun R. Acharya.
All rights reserved. Use of the code is allowed under the
Artistic License 2.0 terms, as specified in the LICENSE file
distributed with this code, or available from
http://www.opensource.org/licenses/artistic-license-2.0.php
***********************************/

#ifndef _BASELATTICE_

#include <iostream>
#include <exception>
#include <deque>
#include <math.h>
#include <vector>
#include <fstream>
#include <boost/random.hpp>
#include "exception_latticespin.h"

/*
-->LIMITATIONS OF THE PROGRAM<--
1)Only periodic boundary conditons have been implemented
2)Spin dimension can vary, but is (upper) bounded by the
system dimension
3)Integer overflow due to too large systems
http://msdn.microsoft.com/en-us/library/s3f49ktz(VS.80).aspx
The largest unsigned integer = 4294967295
POWER(1625,3) -> 4,291,015,625
POWER(1626,3) -> 4,298,942,376
Therefore 1625 is the largest possible system size for a
straightforward method of indexing.
4)Only cubic structure lattices have been considered
-->END : LIMITATIONS OF THE PROGRAM<--


-->BASIC MECHANISM OF HOLDING THE DATA<--
T denote the data-typeholder for the direction of each spin.
for example if T = int, then we have spin up(+1) and down(-1)
if T = vector<int> of size 3, then we have a spin which can
assume +-1 values in the x,y, and z direction.The permitted
values are :

(1,0,0)
(0,1,0)
(0,0,1)
(-1,0,0)
(0,-1,0)
(0,0,-1)

sidelength = the length of a side of the lattice
dim = number of dimensions
dimofspin = dimensionality of the spin
(spin is bound to the first dimofspin dimensions. Clearly
we have the constraint that dimofspin <= dim)

total number of particles = sidelength^dim
-->END : BASIC MECHANISM OF HOLDING THE DATA<--


-->RECOMMENDED WAY OF STORING THE DATA & NEAREST NEIGHBOURS<--
The fastest data storage mechanism when you consider
read and write speeds is one single long array. Note this
is faster than multidimensional arrays.

Now let us consider the case of short ranged interactions.
If we use multimensional arrays, then accumulating the
nearest neighbours is easy : we simply do +-1 for each
dimension index. In the case of a single long array,
we have to do it in the following manner. Suppose that
L = length of each side of our multidensional cubiod.

1st dimension : 1+- in x : add 1+- to current index
2st dimension : 1+- in y : add L+- to current index
3st dimension : 1+- in z : add L*L+- to current index
...
nth dimension : 1+- in dimension : add L^{n-1} to current index
-->END : RECOMMENDED WAY OF STORING THE DATA & NEAREST NEIGHBOURS<--


-->DATA STORAGE AND SPEED CONSIDERATIONS<--
Note that for speed reasons the data is stored on the stack.
Since the user inputs the sidelength, and the dimension (thus
giving a total number of particles equal to sidelength^dimension)
, and since the total number particles cannot be evaluated at
compile time using any of the inbuilt functions, we use
template meta-programming to calculate the number of particles
at compile time, thus allowing us to construct the appropriately
size array on stack.

In Visual studio, If the program does not compile for large system sizes,
then you can go to :

a)Project properties
b)Linker
c)System
d)Specify the Stack Reserve Limit.

However if you do not want to do this you can allocate
memory on the heap, however this will slow things down.
-->END : DATA STORAGE AND SPEED CONSIDERATIONS<--
*/


//arjun : This is to be deleted at the end of testing
//Remember that this is present in all files that use this
//header
//#define _TEST

namespace global_vars
{
    const double tolerance =0.00000001;
}

//Because the exceptions in gcc and Visual studio are slighly
//different, we use Boost exception to harmonise exception handling


//**********TEMPLATE METAPROGRAMMING*****************//
//evaluate_numspins is purely to evaluate the actual
//number of spins at compile time. Note that the reason we separate
//it from the base_latticefunctions class is because the relevant
//variable has to be public for the purposes of
//template metaprogramming.
//the reason we use template metaprogramming is so that
//the data storage can be implemented on stack (see below)
//stack storage is the fastest
template<int sidelength, int dim>
class evaluate_numspins
{
public:
    //Note localsize is set by template metaprogramming to be
    //equal to pow(sidelength,dim).
    //We do things in this convoluted way because
    //the pow() function can not be used to evaluate desired
    //data at compile time.
    enum {localsize = sidelength * evaluate_numspins<sidelength,dim-1>::localsize};
};

//To terminate the template metaprogramming
template<int sidelength>
class evaluate_numspins<sidelength,0>
{
public:
    enum {localsize = 1};
};
//**********end of TEMPLATE METAPROGRAMMING************//


//****************THE BASE CLASS***********************//
//Note that base_latticefunctions is an abstract class which is used to
//declare the essential functions required for operations on the lattice.
class base_latticefunctions
{
public:
    virtual void alignspinsinxdir(void)=0;
    virtual void description()=0;//Output to the terminal a simple
    //description of what the class does, and how it is different
    //from other variants
    virtual double calcglobalenergy()=0;
    //virtual void latticeupdate()=0;
    virtual void latticeupdate_sitespecified()=0;
    virtual void spinupdate()=0;//updates a randomly chosen site
    virtual void spinupdate(int site)=0;//updates a specified site
    //Returns the energy change of the system if spin at site_change has its
    //spin changed such that new_component assumes a value "value".
    virtual double perturb(int site_change, int new_component, int value)=0;
    //This checks to see whether the energy agrees with the calculated global
    //energy. Note that if there is a mismatch, then an exception is throw.
    virtual void check()=0;//calculates the total energy and sees if this agrees
    //with the internal state
    //This calculates the average magnetisation for the whole lattice
    virtual void calc_average_magnetisation()=0;
    virtual double getenergy(void)=0;
    virtual double getmagnetisation(int component)=0;
};
//****************end of THE BASE CLASS***********************//

//**********DATA HOLDER*******************//
template<typename T, int sidelength, int dim>
class dataholder : public evaluate_numspins<sidelength,dim>
{
protected:
    T spins[evaluate_numspins<sidelength,dim>::localsize];
    //Note that you can make variants of this such as
    //vector<T> spins(localsize); etc....
    //As mentioned before, it was chosen to do things this way
    //since data storage on stack is faster
    //Also visual studio does not need "evaluate_numspins<sidelength,dim>::"
    //But gcc does.
};
//**********END of DATA HOLDER************//

//**********THE BASE CLASS WITH DATA HOLDER************//
//Note that in this class some variables were defined, which
//could really have been defined in their respective functions.
//However we defined them in the class to get around the time
//associated with their construction and destruction. To really
//see whether this helps things an analysis of the timescales
//needs to be done.
template<typename T, int sidelength, int dim, int dimofspin>
class baselattice_with_functions_and_dataholder :
            private dataholder<T,sidelength,dim>,
            public base_latticefunctions
{
protected:
    //So you can use the word spins without using the
    //"dataholder<T,sidelength,dim>::" keyword or this->
    using dataholder<T,sidelength,dim>::spins;
    const int numspins;//total number of spins = pow(sidelength,dim)

    double magfield[dim];//Vector containing the magnetic field

    double J;//Coupling constant
    double mu;//The magnetic moment
    std::vector<double> magfield_input;//Simply to take in the input.
    //we then store in array magfield[dim], for which accessing
    //times are better
    double kT;//Scaled temperature

    //Variables for magnetisation calculation
    double average_mag[dimofspin];
    double average_mag_temporary[dimofspin];
    double temp_spin[dimofspin];//Just to save the time associated with
    //creation and destruction of variables. see the spin update functions.

    //Note that there was a temptation to wrap up the random numbers generation
    //into a seperate class. However this slows things down very slighly. Furthermore
    //the array of random numbers (e.g. one for site, one for spin orientation etc)
    //really mimics the structure of the underlying lattice, and therefore is
    //well placed to be defined within this class.

    int seed_spin;//choosing spin site (i.e. one of numspins)
    int seed_orient;//choosing spin orientation (i.e. one of dimofspin)
    int seed_plusminus;//Whether the spin assumes value +1 of -1
    //in relevant direction
    int seed_cont;//used to determine whether to accept or reject
    //the trial move in the Metropolis algorithm

    boost::mt19937 spin_boost_engine;
    boost::mt19937 orient_boost_engine;
    boost::mt19937 plusminus_boost_engine;
    boost::mt19937 cont_boost_engine;

    boost::uniform_int<> spin_boost_distribution;
    boost::uniform_int<> orient_boost_distribution;
    boost::uniform_int<> plusminus_boost_distribution;
    boost::uniform_real<> cont_boost_distribution;

    boost::variate_generator<boost::mt19937&,boost::uniform_int<> > spin_die;
    boost::variate_generator<boost::mt19937&,boost::uniform_int<> > orient_die;
    boost::variate_generator<boost::mt19937&,boost::uniform_int<> > plusminus_die;
    boost::variate_generator<boost::mt19937&,boost::uniform_real<> > cont_die;

    double energy;

    //The below are variables which are purely to be used in the spinupdate
    //and are defined here for reasons cited above (saving time)
    int spu_trial_site, spu_trial_orient, spu_trial_plusminus;
    double spu_trial_cont, spu_energydiff, spu_scaledtemp;

public:
    baselattice_with_functions_and_dataholder();//declared but not defined
    //i.e. forced to use the parameterised constructor

    baselattice_with_functions_and_dataholder(
        double i_J,
        double i_mu,
        std::vector<double> i_magfield_input,
        double i_kT
    );

    //***THE PURE VIRTUAL FUNCTIONS*****

    void alignspinsinxdir(void);
    void spinupdate();
    void spinupdate(int site);
    void latticeupdate();
    void latticeupdate_sitespecified();
    void calc_average_magnetisation();
    void check();
    double getenergy(void);
    double getmagnetisation(int component);
    double perturb(int site_change, int trial_component, int trial_value);
    double calcglobalenergy();

    //***UTILITIES*****

    int dot_prod(int a[dimofspin], int b[dimofspin]);
    double dot_prod(int a[dimofspin], double b[dimofspin]);
    void set_vec(int toset[dimofspin], int val[dimofspin]);
    void randomise_lattice(void);
    void getcoords(int site, int coords[dim]);
    int get_site(int coords[dim]);
    int dotprodvecwithneighbours(int a[dimofspin], int coords[dim]);
    //This calculation assumes interaction with only first nearest neighbours
    double calc_local_energy(int a[dimofspin],int coords[dim]);

    //Given the site, this sets the component to equal value.
    //There are the following restrictions:
    //value = {-1,+1} , 0<=component<dimofspin
    void set_spin(int site, int component, int value);

    //Purely for printing STL containers
    template<typename U> void localprint(U arg);

    //***TESTING FUNCTIONS*****
    //This generates random numbers from spin_die and stores
    //the results in the file "rand_spin.txt"
    void test_spin_rand(int nrand);
    //This generates random numbers from orient_die and stores
    //the results in the file "rand_orient.txt"
    void test_orient_rand(int nrand);
    //similar idea to the above
    void test_plusminus_rand(int nrand);
    //similar idea to the above
    void test_cont_rand(int nrand);
    //This sums the local energies. Just check that the sum in the
    //case of the initial state is equal to 2* initial state energy,
    //for the case of short ranged interactions
    double test_sum_of_local_energies(void);
    int numofspins_in_this_dim_plus(int this_dim);
    int numofspins_in_this_dim_minus(int this_dim);
    //Performs many lattice sweeps. After each lattice sweep, the statistics of
    //of the directions of the spins is collected and outputted to the file
    void test_stats_after_randomising(int lattice_sweeps);
    void test_output_direction_stats(void);
    //What this does is to flip one spin and calculate the total
    //energy. This is only to be performed after the system has
    //been put into the ground state. Then such an operation should
    //change raise the energy by 6J - 2mu, when B=<1,1,1>
    double test_groundenergy_with_one_spin_flip();
    void test_counting_the_spins(void);
};

template<typename T, int sidelength, int dim, int dimofspin>
baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::baselattice_with_functions_and_dataholder(
    double i_J,
    double i_mu,
    std::vector<double> i_magfield_input,
    double i_kT
):
        numspins(evaluate_numspins<sidelength,dim>::localsize),
        J(i_J),
        mu(i_mu),
        magfield_input(i_magfield_input),
        kT(i_kT),
        seed_spin((unsigned)time('\0')),
        seed_orient((unsigned)time('\0')+(rand()%1000)),
        seed_plusminus((unsigned)time('\0')+(rand()%10000)),
        seed_cont((unsigned)time('\0')+(rand()%100000)),
        spin_boost_engine((long)seed_spin),
        orient_boost_engine((long)seed_orient),
        plusminus_boost_engine((long)seed_plusminus),
        cont_boost_engine((long)seed_cont),
        spin_boost_distribution(boost::uniform_int<>(0,numspins-1)),//Remember generates nums, including 0 and numspins-1
        orient_boost_distribution(boost::uniform_int<>(0,dimofspin-1)),
        plusminus_boost_distribution(boost::uniform_int<>(0,1)),
        cont_boost_distribution(boost::uniform_real<>(0,1)),
        spin_die(boost::variate_generator<boost::mt19937&,boost::uniform_int<> >(spin_boost_engine,spin_boost_distribution)),
        orient_die(boost::variate_generator<boost::mt19937&,boost::uniform_int<> >(orient_boost_engine,orient_boost_distribution)),
        plusminus_die(boost::variate_generator<boost::mt19937&,boost::uniform_int<> >(plusminus_boost_engine,plusminus_boost_distribution)),
        cont_die(boost::variate_generator<boost::mt19937&,boost::uniform_real<> >(cont_boost_engine,cont_boost_distribution))
{
    //Now storing the magnetic field into a raw array.
    if (int(magfield_input.size())!=dim)
    {
        throw exception_lattice("ising3d constructor : The dimensionality of the \
                                input vector does not match the dimensionality of \
                                the system\n");
    }
    for (int i=0; i<dim; ++i)
    {
        magfield[i] = magfield_input[i];
    }

    alignspinsinxdir();
    calc_average_magnetisation();
}


template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::alignspinsinxdir(void)
{
    //initialising spins to point posively in the first dimension
    for (int i=0; i<numspins; ++i)
    {
        spins[i][0] = 1;
        for (int j=1; j<dimofspin; ++j)
        {
            spins[i][j]=0;
        }
    }
}


template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_spin_rand(int nrand)
{
    std::ofstream out("rand_spin.txt");
    for (int i=0; i<nrand; ++i)
    {
        out<<spin_die()<<std::endl;
    }
    out.close();
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_orient_rand(int nrand)
{
    std::ofstream out("rand_orient.txt");
    for (int i=0; i<nrand; ++i)
    {
        out<<orient_die()<<std::endl;
    }
    out.close();
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_plusminus_rand(int nrand)
{
    std::ofstream out("rand_plusminus.txt");
    for (int i=0; i<nrand; ++i)
    {
        out<<plusminus_die()<<std::endl;
    }
    out.close();
}


template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_cont_rand(int nrand)
{
    std::ofstream out("rand_cont.txt");
    for (int i=0; i<nrand; ++i)
    {
        out<<cont_die()<<std::endl;
    }
    out.close();
}


template<typename T, int sidelength, int dim, int dimofspin>
int baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::dot_prod(int a[dimofspin], int b[dimofspin])
{
    int ret = 0;
    for (int i=0; i<dimofspin; ++i)
    {
        ret += a[i]*b[i];
    }
    return ret;
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::dot_prod(int a[dimofspin], double b[dimofspin])
{
    double ret = 0.;
    for (int i=0; i<dimofspin; ++i)
    {
        ret += a[i]*b[i];
    }
    return ret;
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::set_vec(int toset[dimofspin], int val[dimofspin])
{
    for (int i=0; i<dimofspin; ++i)
    {
        toset[i]= val[i];
    }
}

template<typename T, int sidelength, int dim, int dimofspin>
template<typename U>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::localprint(U arg)
{
    typename U::iterator i;
    i = arg.begin();
    while (i<arg.end())
    {
        std::cout<<*i<<std::endl;
        i++;
    }
}


template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::set_spin(int site, int component, int value)
{
#ifdef _TEST
    if (component<0||component>=dimofspin)
    {
        throw exception_lattice("set_spin : component is out of bounds\n");
    }
    if (value != -1 && value != 1)
    {
        throw exception_lattice("set_spin : value being set is not permitted\n");
    }
    if (site < 0 || site >= numspins)
    {
        throw exception_lattice("set_spin : site is out of bounds\n");
    }
#endif

    for (int i=0; i<dimofspin; ++i)
    {
        spins[site][i]=0;
    }
    spins[site][component] = value;
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::randomise_lattice(void)
{
    alignspinsinxdir();
    for (int i=0; i<numspins; ++i)
    {
        int rand_component = orient_die();
        int rand_value = 2*plusminus_die()-1;
#ifdef _TEST
        if (rand_component<0||rand_component>=dimofspin)
        {
            throw exception_lattice("randomise_lattice : generated component out of bounds\n");
        }
        if (rand_value!= -1 && rand_value !=1)
        {
            throw exception_lattice("randomise_lattice : genenerated value not permitted\n");
        }
#endif
        set_spin(i,rand_component,rand_value);
    }
    calc_average_magnetisation();
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::getenergy(void)
{
    return energy;
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::spinupdate()
{

    //Generating the trial parameters
    spu_trial_site = spin_die();
    spu_trial_orient = orient_die();
    spu_trial_plusminus = 2*plusminus_die()-1;
    spu_trial_cont = cont_die();

    //Calculating the potential energy change due to a perturbation
    spu_energydiff = perturb(spu_trial_site, spu_trial_orient, spu_trial_plusminus);
    spu_scaledtemp = spu_energydiff /kT;
    //The accept or reject part of the algorithm
    if (spu_trial_cont < std::exp(-spu_scaledtemp))
    {

        //*****Modifying the magnetisation
        //arjun : need to see how the following affects the speed of the
        //program
        for (int i=0; i<dimofspin; ++i)
        {
            temp_spin[i] = 0;
        }
        temp_spin[spu_trial_orient] = spu_trial_plusminus;

        for (register int j=0; j<dimofspin; ++j)
        {
            average_mag_temporary[j] = numspins*average_mag[j];
            average_mag_temporary[j] -= spins[spu_trial_site][j];
            average_mag_temporary[j] += temp_spin[j];
            average_mag[j] = average_mag_temporary[j]/double(numspins);
        }
        //*****end of magnetisation
        set_spin(spu_trial_site,spu_trial_orient,spu_trial_plusminus);
        energy = energy + spu_energydiff;
    }
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::spinupdate(int site)
{
    //Generating the trial parameters
    spu_trial_orient = orient_die();
    spu_trial_plusminus = 2*plusminus_die()-1;
    spu_trial_cont = cont_die();
    //Calculating the potential energy change due to a perturbation
    spu_energydiff = perturb(site, spu_trial_orient, spu_trial_plusminus);
    spu_scaledtemp = spu_energydiff /kT;
    //The accept or reject part of the algorithm
    if (spu_trial_cont < std::exp(-spu_scaledtemp))
    {

        //*****Modifying the magnetisation
        //arjun : need to see how the following affects the speed of the
        //program
        for (int i=0; i<dimofspin; ++i)
        {
            temp_spin[i] = 0;
        }
        temp_spin[spu_trial_orient] = spu_trial_plusminus;

        for (register int j=0; j<dimofspin; ++j)
        {
            average_mag_temporary[j] = numspins*average_mag[j];
            average_mag_temporary[j] -= spins[site][j];
            average_mag_temporary[j] += temp_spin[j];
            average_mag[j] = average_mag_temporary[j]/double(numspins);
        }
        //*****end of magnetisation

        set_spin(site,spu_trial_orient,spu_trial_plusminus);
        energy = energy + spu_energydiff;
    }
}


template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::latticeupdate()
{
    for (int i=0; i<numspins;++i)
    {
        spinupdate();
    }
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::latticeupdate_sitespecified()
{
    for (int i=0; i<numspins;++i)
    {
        spinupdate(i);
    }
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::check()
{
    double calc_energy = calcglobalenergy();
    if (fabs(calc_energy-energy)>global_vars::tolerance)
    {
        throw exception_lattice("check : mismatch of energies\n");
    }

    double mag[dimofspin];

    for (int j=0; j<dimofspin; ++j)
    {
        mag[j]=average_mag[j];
    }

    calc_average_magnetisation();

    for (int j=0; j<dimofspin; ++j)
    {
        if (fabs(mag[j]-average_mag[j])>global_vars::tolerance)
        {
            throw exception_lattice("check : mismatch of magnetisation\n");
        }
    }

}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::getcoords(int site, int coords[dim])
{
    /*
    Note that in the original algorithm we had the following :
    x = site%sidelength;
    y = ((site - x)/sidelength)%sidelength;
    z = ((((site - x)/sidelength) - y)/sidelength)%sidelength;
    The below is a generalisation to arbitrary coordinates
    */

    int running_site = site;
    for (int i_dim = 0; i_dim < dim; ++i_dim)
    {
        coords[i_dim] = running_site%sidelength;
        running_site = (running_site - coords[i_dim])/sidelength;
    }
}


template<typename T, int sidelength, int dim, int dimofspin>
int baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::get_site(int coords[dim])
{
    /*
    The original algorithm was as follows :

    template<int sidelength,int dimofspin>
    int ising3d<sidelength,dimofspin>::get_site(int x, int y, int z){
    #ifdef _TEST
    if((x+sidelength)<0||(y+sidelength)<0||(z+sidelength)<0){
    throw exception("get_site : _TEST : {x||y||z} < 0.\n");
    }
    #endif
    //Note that in the below we add sidelength just to get around
    //negative numbers which one encounters for nearest neighbours
    //at the faces of the cuboid
    int index = ((x+sidelength)%sidelength) +
    ((y+sidelength)%sidelength)*sidelength +
    ((z+sidelength)%sidelength)*sidelength_sq;
    #ifdef _TEST
    if(index<0||index>=numspins){
    throw exception_lattice("get_site : return index out of range\n");
    }
    #endif

    return index;
    }
    */

#ifdef _TEST
    for (int i=0; i<dim; ++i)
    {
        if ((coords[i]+sidelength)<0)
        {
            std::cout<<"get_site : coordinates "<<i<<" is less than zero\n";
            throw exception_lattice("get_site : one of the coordinates is less than zero\n");
        }
    }
#endif
    //Note that in the below we add sidelength just to get around
    //negative numbers which one encounters for nearest neighbours
    //at the faces of the cuboid

    //int index = ((x+sidelength)%sidelength) +
    //	((y+sidelength)%sidelength)*sidelength +
    //	((z+sidelength)%sidelength)*sidelength_sq;

    int index = 0;
    for (int i=0; i<dim; ++i)
    {
        index = index + ((coords[i]+sidelength)%sidelength)*int(pow(double(sidelength),double(i)));
    }

#ifdef _TEST
    if (index<0||index>=numspins)
    {
        throw exception_lattice("get_site : return index out of range\n");
    }
#endif

    return index;
}

template<typename T, int sidelength, int dim, int dimofspin>
int baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::dotprodvecwithneighbours(int a[dimofspin], int coords[dim])
{
    int ret = 0;

    int site;
    int l_coords[dim];

    for (int i=0; i<dim; ++i)
    {
        l_coords[i] = coords[i];
    }

    for (int i=0; i<dim; ++i)
    {

        l_coords[i] += 1;//The NN in the positive directions
        site = get_site(l_coords);
        l_coords[i] -= 1;
        ret += dot_prod(a,spins[site]);

        l_coords[i] -= 1;//The NN in the negative directions
        site = get_site(l_coords);
        l_coords[i] += 1;
        ret += dot_prod(a,spins[site]);

    }

    return ret;
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::calc_local_energy(int a[dimofspin],int coords[dim])
{
    double energy = -J*dotprodvecwithneighbours(a,coords) -(mu *  dot_prod(a,magfield));
    return energy;
}



template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::perturb(int site_change, int trial_component, int trial_value)
{

    double current_local_energy=0.;
    double trial_local_energy=0.;
    int l_coords[dim];

#ifdef _TEST
    if (trial_component<0||trial_component>=dimofspin)
    {
        throw exception_lattice("perturb : component is out of bounds\n");
    }
    if (trial_value != -1 && trial_value != 1)
    {
        throw exception_lattice("perturb : value being set is not permitted\n");
    }
#endif

    int newspin[dimofspin];
    for (int i=0; i<dimofspin; ++i)
    {
        newspin[i] = 0;
    }
    newspin[trial_component] = trial_value;

    getcoords(site_change,l_coords);

    current_local_energy = calc_local_energy(spins[site_change],l_coords);
    trial_local_energy = calc_local_energy(newspin,l_coords);

    return (trial_local_energy - current_local_energy);
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::calc_average_magnetisation()
{
    for (int j=0; j<dimofspin; ++j)
    {
        average_mag[j] = 0.;
    }

    for (int i=0; i<numspins; ++i)
    {
        for (int j=0; j<dimofspin; ++j)
        {
            average_mag[j] += double(spins[i][j]);
        }
    }

    for (int j=0; j<dimofspin; ++j)
    {
        average_mag[j] = average_mag[j] / double(numspins);
    }
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::getmagnetisation(int component)
{
    if (component >= dimofspin)
    {
        throw exception_lattice("getmagnetisation : trying to index beyond bounds of array\n");
    }
    return average_mag[component];
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_sum_of_local_energies(void)
{
    double sum = 0;
    alignspinsinxdir();
    int coords[dim];
    for (int i=0; i<numspins; ++i)
    {
        getcoords(i, coords);
        sum += calc_local_energy(spins[i],coords);
    }
    return sum;
}

template<typename T, int sidelength, int dim, int dimofspin>
int baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::numofspins_in_this_dim_plus(int this_dim)
{
    int num = 0;
    int vec[dimofspin];
    for (int i=0; i<dimofspin; ++i)
    {
        vec[i] = 0;
    }
    vec[this_dim] = 1;

    for (int i=0; i<numspins; ++i)
    {
        if (dot_prod(spins[i],vec)== 1)
        {
            num += 1;
        }
    }
    return num;

}


template<typename T, int sidelength, int dim, int dimofspin>
int baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::numofspins_in_this_dim_minus(int this_dim)
{
    int num = 0;
    int vec[dimofspin];
    for (int i=0; i<dimofspin; ++i)
    {
        vec[i] = 0;
    }
    vec[this_dim] = -1;

    for (int i=0; i<numspins; ++i)
    {
        if (dot_prod(spins[i],vec)== 1)
        {
            num += 1;
        }
    }
    return num;
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_stats_after_randomising(int lattice_sweeps)
{

    std::vector<int> hist(2*dimofspin);

    std::ofstream out("output_stats_after_randomising.txt");

    std::fill(hist.begin(),hist.end(),0);

    for (int i=0; i<lattice_sweeps; ++i)
    {
        randomise_lattice();
        for (int j=0; j<dimofspin; ++j)
        {
            hist[j] += numofspins_in_this_dim_plus(j);
            hist[j+dimofspin] += numofspins_in_this_dim_minus(j);
        }
    }

    for (int i=0; i<2*dimofspin;++i)
    {
        out<<hist[i]<<std::endl;
    }

    out.close();
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_output_direction_stats(void)
{
    std::cout<<"If one indexes in such a way that the first dimension is 0, then :\n";
    for (int i=0; i<dimofspin; ++i)
    {
        std::cout<<"number of spins in positive "<<i<<" dimension = "<<numofspins_in_this_dim_plus(i)<<std::endl;
        std::cout<<"number of spins in negative "<<i<<" dimension = "<<numofspins_in_this_dim_minus(i)<<std::endl;
    }
}

//Note that this calculation
template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::calcglobalenergy()
{
    double lenergy = 0.;

    //Note that the below is of size dim and not dimofspin
    int coords_nn[dim];
    int nn_site;

    for (int l_site=0; l_site < numspins; ++l_site)
    {
        //plus dimension. Note we only take the positive dimension, because otherwise
        //if we also sum over the negative dimension then we will end up double
        //counting the energy
        for (int l_dim=0; l_dim < dim; ++l_dim)
        {
            getcoords(l_site, coords_nn);
            coords_nn[l_dim] += 1;
            nn_site = get_site(coords_nn);
            lenergy += (-J*dot_prod(spins[l_site],spins[nn_site]));
        }
        //Now for the magnetic component
        lenergy += (-mu*dot_prod(spins[l_site],magfield));
    }

    return lenergy;
}

template<typename T, int sidelength, int dim, int dimofspin>
double baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_groundenergy_with_one_spin_flip()
{
    double te;
    std::vector<double> energies;
    for (int i=0; i<numspins; ++i)
    {
        spins[i][0] *= -1;//Remember only one of the
        te = calcglobalenergy();
        energies.push_back(te);
        spins[i][0] *= -1;
    }
    std::vector<double>::iterator iter_min = min_element(energies.begin(), energies.end());
    std::vector<double>::iterator iter_max = max_element(energies.begin(), energies.end());
    if (iter_min != iter_max)
    {
        throw exception_lattice("test_groundenergy_with_one_spin_flip : Starting from the ground state\
                                (all aligned spins), a spin flip of an arbitrary site does not yield \
                                yield the same energy irrespective of the choice of site. This is clearly\
                                an error in the formulation.");
    }
    return *iter_max;
}

template<typename T, int sidelength, int dim, int dimofspin>
void baselattice_with_functions_and_dataholder<T,sidelength,dim,dimofspin>::test_counting_the_spins(void)
{
    alignspinsinxdir();

    std::cout<<"With the system having all spins aligned parrallel\
    in the x direction, the spin orientation is as follows : \n";
    test_output_direction_stats();
    std::cout<<std::endl;
}


//*********end of THE BASE CLASS WITH DATA HOLDER************//

#endif //_BASELATTICE_
