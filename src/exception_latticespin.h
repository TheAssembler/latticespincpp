/**********************************
Copyright (c) 2008 Arjun R. Acharya.
All rights reserved. Use of the code is allowed under the
Artistic License 2.0 terms, as specified in the LICENSE file
distributed with this code, or available from
http://www.opensource.org/licenses/artistic-license-2.0.php
***********************************/

#ifndef _EXCEPTION_LATTICE
#define _EXCEPTION_LATTICE

#include <iostream>
#include <exception>

class exception_lattice : public std::exception
{
    const char * m_what;
public :
    exception_lattice():m_what(NULL){}
    exception_lattice(const char *const &p);
    const char* what() const throw();
    exception_lattice(const exception_lattice& p);
    bool isnull(void) const;
};

exception_lattice::exception_lattice(const char *const &p)
{
    if (p!=NULL)
    {
        size_t size = strlen(p) + 1;
        m_what = static_cast< char * >( malloc( size ) );
        if (m_what!=NULL)
        {
            for (int index = 0; index <= int(size);++index)
            {
                const_cast<char*>(m_what)[index] = p[index];
            }
        }
    }
    else
    {
        m_what = NULL;
    }
}

const char* exception_lattice::what() const throw()
{
    if ( m_what != NULL )
        return m_what;
    else
        return "Unknown exception";
}

bool exception_lattice::isnull(void) const
{
    if (m_what == NULL)
    {
        return true;
    }
    else
    {
        return false;
    }
}

exception_lattice::exception_lattice(const exception_lattice& p)
{
    if (p.isnull()==false)
    {
        const char *lp = p.what();
        size_t size = strlen(lp) + 1;
        m_what = static_cast< char * >( malloc( size ) );
        if (m_what!=NULL)
        {
            for (int index = 0; index <= int(size);++index)
            {
                const_cast<char*>(m_what)[index] = lp[index];
            }
        }
    }
    else
    {
        m_what = NULL;
    }
}

#endif

//This is a workaround for some strange behaviour which
//is observed when Visual Studio Language extensions
//is turned off. Make sure that it remains at the bottom
//of the code
namespace
{

};
