/**********************************
Copyright (c) 2008 Arjun R. Acharya.
All rights reserved. Use of the code is allowed under the
Artistic License 2.0 terms, as specified in the LICENSE file
distributed with this code, or available from
http://www.opensource.org/licenses/artistic-license-2.0.php
***********************************/

#ifndef _SPECIALIZATIONS_
#include "base_latticespin.h"
#include <fstream>
#include <vector>
#include <time.h>
#include <fstream>
#include <algorithm>
#include <math.h>
#include <exception>

//This is for the calculation of elliptic integrals in the 2D
//case.
#include <boost/math/special_functions/ellint_1.hpp>

/*
The hamiltonian of this model is assumed to be given by :
H = -J \sum_<i,j> S_i S_j - mu B \sum_i S_i

where <i,j> denotes summation over first nearest
neighbours in the case of short ranged interations.
*/

const int dim = 3;//The number of dimensions

//Note that because arrays cannot be initialised in the
//initialization list, we force the class to input the magnetic
//field as a vector


//dimofspin denotes the dimensionality of the direction the spin
//can point in.
//if dimofspin = 1, then spin up or down
//if dimofspin = 2, then it can point in 4 directions obtained by
//90degree rotations in the plane.
//if dimofspin = 3, then it can point in 1 of 6 directions
//note that we have the constraint that dimofspin <= dim
//also note that we feed in int[dim] into baselattice_with_functions_and_dataholder
//because this denotes the dimensionality that the spin can point
//in. Also note that in the below we choose "int[dim]" instead of
//"int[dimofspin]" because when selecting from the state of possible
//spins we take into account the fact that the degrees of freedom
//of the spin only lie within the first dimofspin dimensions.
//Note also that strictly you could incorporate the spin degrees of freedom
//within the one dimensional array to make it even faster, but we stop that here.


//*********************START OF THE 3D ISING MODEL*******************//

template<int sidelength, int dimofspin>
class ising3d : public baselattice_with_functions_and_dataholder<int[dim],sidelength,dim,dimofspin>
{

protected:
    std::ofstream output_params;
public:

    ising3d();//declared but not defined

    ising3d(
        double i_J,
        double i_mu,
        std::vector<double> i_magfield_input,
        double i_kT
    );

    //***THE PURE VIRTUAL FUNCTIONS*****
    virtual void description();
};

template<int sidelength,int dimofspin>
ising3d<sidelength,dimofspin>::ising3d(
    double i_J,
    double i_mu,
    std::vector<double> i_magfield_input,
    double i_kT
):
        baselattice_with_functions_and_dataholder<int[dim],sidelength,dim,dimofspin>(
            i_J,
            i_mu,
            i_magfield_input,
            i_kT
        ),
        output_params("output_parameters.txt")
{
    output_params<<"seed_spin\t=\t"<<this->seed_spin<<std::endl;
    output_params<<"seed_orient\t=\t"<<this->seed_orient<<std::endl;
    output_params<<"seed_plusminus\t=\t"<<this->seed_plusminus<<std::endl;
    output_params<<"seed_cont\t=\t"<<this->seed_cont<<std::endl;
    output_params.close();

    this->energy = this->calcglobalenergy();
}

template<int sidelength,int dimofspin>
void ising3d<sidelength,dimofspin>::description()
{
    std::cout<<"This class simulates the 3D Ising model with magnetic"
    <<" field\n"
    <<"-->Short Ranged Interactions\n"
    <<"-->Periodic boundary conditions\n"
    <<"-->Arbitrary strength and arbitrary direction magnetic field\n"
    <<"-->Spin can point in one of six directions\n";


    //arjun : work has to be done on this function e.g.
    //define the parameters and how to specify the parameters
}
//*********************END OF THE 3D ISING MODEL*******************//

//*********************START OF THE 2D ISING MODEL*******************//
/*
Note that the below is a real hack. What we do is to define a 3D model,
Then the 2D model corresponds to one of the planes of the 3d model.
Note that all the other spins in all the other planes are set to zero,
Also when we attempt trial moves, we only attempt on the corresponding
plane. Furthermore the total energy is only calculated by considering
the relevant plane
*/

//template<int sidelength, int dimofspin>
//class ising3d : public baselattice_with_functions_and_dataholder<int[dim],sidelength,dim>{

//Note that for the 2d case, we inherit from the 3d class. However
//we make the dotprodvecwithneighbours, which is called in
//"calc_local_energy" virtual so that only neighbours
//in the same plane are considered when calculating the local energy.

//template<int sidelength, int dimofspin>
//class ising2d : public ising3d<sidelength, dimofspin>{
const int dim2d = 2;

template<int sidelength, int dimofspin>
class ising2d : public baselattice_with_functions_and_dataholder<int[dim2d],sidelength, dim2d, dimofspin>
{
public:
    ising2d();
    ising2d(
        double i_J,
        double i_mu,
        std::vector<double> i_magfield_input,
        double i_kT
    );

    virtual void description();
    //virtual void latticeupdate_sitespecified();
    //virtual void latticeupdate(){throw exception("latticeupdate not defined for ising2d. Use latticeupdate_sitespecified\n");}
    double analytical_internal_energy_per_particle(void);
};

template<int sidelength, int dimofspin>
ising2d<sidelength,dimofspin>::ising2d(
    double i_J,
    double i_mu,
    std::vector<double> i_magfield_input,
    double i_kT
) :
        baselattice_with_functions_and_dataholder<int[dim2d],sidelength, dim2d, dimofspin>(i_J,i_mu,i_magfield_input,i_kT)
{
    if (dimofspin>2)
    {
        throw exception_lattice("ising2d() : Dimensionality of spin in 2d model greater than 2\n");
    }

    this->energy = this->calcglobalenergy();
}


template<int sidelength,int dimofspin>
void ising2d<sidelength,dimofspin>::description()
{
    std::cout<<"This class simulates the 2D Ising model with magnetic"
    <<" field\n"
    <<"-->Short Ranged Interactions\n"
    <<"-->Periodic boundary conditions\n"
    <<"-->Arbitrary strength and arbitrary direction magnetic field\n"
    <<"-->Spin can point in one of six directions\n";


    //arjun : work has to be done on this function e.g.
    //define the parameters and how to specify the parameters
}

template<int sidelength,int dimofspin>
double ising2d<sidelength,dimofspin>::analytical_internal_energy_per_particle(void)
{
    /*
    http://www.boost.org/doc/libs/1_35_0/libs/math/doc/sf_and_dist/html/math_toolkit/special/ellint/ellint_1.html
    */
    /*
    the formulas below were taken from :
    "Theory of the Ising model of ferromagnetism"
    */
    double K = this->J/this->kT;
    const double pi = 3.14159265358979323846;

    double a = 2.*K;
    double sa = sinh(a);
    double ca = cosh(a);
    double ta = (sa/ca);

    double b = 2.*pow(ta,2.0) - 1.;

    double k1 = 2.*sa/(ca*ca);

    double integral = boost::math::ellint_1(k1);

    //Note in the below we have 2/pi not 1/(2*pi) as may seem the case
    //on first reading of the paper
    double bb = 2*b*integral/pi;

    double c = 1.+bb;

    //double internal_energy = -J/tanh(a*c);
    //From first reading of the paper it might seem that we should have "double internal_energy = -J/tanh(a*c);"
    //However I think that the below is correct.
    double internal_energy = -(this->J/tanh(a))*c;
    return internal_energy;
}



//*********************END OF THE 2D ISING MODEL*******************//

#endif//end of _SPECIALIZATIONS_
