instructions for code :

You can build the code in CodeBlocks, which is an open source IDE.
You just create a console application, and then add these files to
the project.You also need to download boost. For example if you
download boost to :

C:\Program Files\boost_1_37_0\boost_1_37_0

with this directory containing the "boost", "docs", and "libs"
directories, then you do :

1)right click on project
2)Build options
3)Search Directories
4)Add "C:\Program Files\boost_1_37_0\boost_1_37_0"

Then you should be able to build and run the project.